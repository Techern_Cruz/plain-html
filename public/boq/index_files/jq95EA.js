

$(document).ready(function() {  
	
		$("#paymentStartDate, #paymentEndDate, #paymentDate, #endDate").datepicker({ 
			appendText: 'DD/MM/YYYY',
			showOn: 'button',
			buttonImageOnly: true,
			buttonImage: 'images/icons/calendar.gif',
			buttonText: '',
			dateFormat: 'dd/mm/yy',
			nextText: "&nbsp;&nbsp;&nbsp;&nbsp;",
			prevText: "&nbsp;&nbsp;&nbsp;&nbsp;",
			minDate: '+0',
			maxDate: '+1y'
	});

	$("#accessStartDate, #accessEndDate").datepicker({ 
			appendText: 'DD/MM/YYYY',
			showOn: 'button',
			buttonImageOnly: true,
			buttonImage: 'images/icons/calendar.gif',
			buttonText: '',
			dateFormat: 'dd/mm/yy',
			nextText: "&nbsp;&nbsp;&nbsp;&nbsp;",
			prevText: "&nbsp;&nbsp;&nbsp;&nbsp;",
			minDate: '+0',
			maxDate: '+1y'
	});

	$("#searchStartDate, #searchEndDate").datepicker({ 
			appendText: 'DD/MM/YYYY',
			showOn: 'button',
			buttonImageOnly: true,
			buttonImage: 'images/icons/calendar.gif',
			buttonText: '',
			dateFormat: "dd/mm/yy",
			nextText: "&nbsp;&nbsp;&nbsp;&nbsp;",
			prevText: "&nbsp;&nbsp;&nbsp;&nbsp;",
			maxDate: "+0"
	});
});

/**********************************************************/
/*					Tooltip script									*/
/**********************************************************/

$(".help").tooltip({ 
	track: true, 
	delay: 0, 
	showURL: false, 
	showBody: " - ", 
	fixPNG: true, 
	opacity: 0.95
});

/**********************************************************/
/*			Progress bar script - FX07						*/
/**********************************************************/

$(document).ready(function(){
	if ($('#FX07').length > 0) {
	var totalTime = getProgressBarTotalTime();
	if(totalTime == 0) {
		$("#progressbar").progressbar({ value: 100 });
	} else {
		$("#progressbar").progressbar({ value: 0 });
	}
	$("#FX07 #btndescTimeExp").hide();
	$("#FX07 #btndesc").show();
	setTimeout ( "updateProgressBar()", 1000 );
	$("#FX07 .pgbtns .btn_").click(function() {
		$(".ui-progressbar .ui-progressbar-value").hide();
		$("#progressbar_timeremaining").hide();
	});
}
});

function updateProgressBar() {
	if ( typeof updateProgressBar.timeRemaining == "undefined" ) {
		updateProgressBar.timeRemaining = getProgressBarTotalTime();
	}
	if(updateProgressBar.timeRemaining != 0) {
		updateProgressBar.timeRemaining = updateProgressBar.timeRemaining - 1;
	}
	$("#progressbar_timeremaining").text(updateProgressBar.timeRemaining);
	if(updateProgressBar.timeRemaining == 1) {
		$("#progressbar_seconds").text("second");
	} else if(updateProgressBar.timeRemaining == 0) {
		$("#progressbar_seconds").text("seconds");
	}
	var value = $("#progressbar").progressbar("option", "value");
	value = value + getProgressBarInterval(getProgressBarTotalTime());
	$("#progressbar").progressbar("option", "value", value);
	if(value < 99.9) {
		setTimeout ( "updateProgressBar()", 1000 );
	} else {
		$("#progressbar").progressbar({ value: 100 });
		$("#FX07 #btndescTimeExp").show();
		$("#FX07 #btndesc").hide();
		$(".pgbtns input:first").addClass("btnd");
		$(".pgbtns input:first").attr("disabled","disabled");
		$(".pgbtns input:first").removeClass("btn_");
	}
}
				
function getProgressBarTotalTime() {
	if ( typeof getProgressBarTotalTime.totalTime == "undefined" ) {
		getProgressBarTotalTime.totalTime = $("#progressbar").text();
		$("#progressbar").text("");
	}
	return getProgressBarTotalTime.totalTime;
}
				
function getProgressBarInterval(totalTime) {
	return (100/totalTime);
}

function stopProgressBar() {
	var value = $("#progressbar").progressbar("option", "value");
	$("#progressbar").progressbar("option", "value", value);
}

/**********************************************************/
/*			FX Calculator Script - FX14 						*/
/**********************************************************/

$(document).ready(function(){
	
	$("#FX14 #currency").change(function() {
		var str = $("#FX14 #currency option:selected").text();
		var optionArray = str.split("(");
		var currency = optionArray[1].substr(0, 3);
		$("#FX14 #foreignToLocal .currencies").text(currency + " to AUD");
		$("#FX14 #localToForeign .currencies").text("AUD to " + currency);
		if($("#FX14 #foreignToLocal .radioBtn").attr("checked") != "") {
			$("#FX14 #amountLbl").text("Amount (" + currency + ")");
		}
		$("#FX14 #convertedAmount").hide();
		$("#FX14 #exchangeRate").hide();
	});

	$("#FX14 .radioBtn").click(function() {
		$(this).attr("checked", "checked");
		if($("#FX14 #foreignToLocal .radioBtn").attr("checked") != "") {
			var currencies = $("#FX14 #foreignToLocal .currencies").text();
		} else {
			var currencies = $("#FX14 #localToForeign .currencies").text();
		}
		var currencyArray = currencies.split(" ");
		var currency = currencyArray[0];
		$("#FX14 #amountLbl").text("Amount (" + currency + ")");
		$("#FX14 #convertedAmount").hide();
		$("#FX14 #exchangeRate").hide();
	});

	$("#FX14 #amount").keydown(function() {
		$("#FX14 #convertedAmount").hide();
		$("#FX14 #exchangeRate").hide();
	});
});
