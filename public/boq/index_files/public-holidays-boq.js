/*
 * Public Holidays, or days when your financial institution will not be processing transactions, should be entered here to allow the javascript pop-up calendar
 * to grey these days out so that they are not selectable.
 * Dates are entered in the format [date, month, year] and each date should be separated by a comma.
 */

/* No default limitations  */
var publicHolidays = [ ];