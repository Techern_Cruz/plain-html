nosub='';
result=true;
submitted=false;
submitClicked = false;
f5Pressed=false;

function getEvent(e) {
	return (typeof(e) == "undefined") ? window.event : e;
}

function eventTarget(e) {
	return (typeof(e.target) == "undefined" ? e.srcElement : e.target);
}

function _over(e) {
	e = getEvent(e);
	var elem = eventTarget(e);
	if (elem == null) return true;
	if (elem.nodeName == "#text")
	  elem = elem.parentNode;
	var cls = elem.className;
	if (cls != "" && (cls.charAt(cls.length-1) == "_" || 
			  cls.indexOf("_ ") > -1)) {
		arr = cls.split(" ");
		str = "";
		for (var i=0; i<arr.length;i++) {
			cls = arr[i];
			if (cls.charAt(cls.length-1) == '_') {
				str=str+" "+cls;
				cls = cls+"o";
			}
			str = str+" "+cls;
		}
		elem.className = str.substring(1,str.length);
	}
	return true;
}

function _out(e) {
	e = getEvent(e);
	var elem = eventTarget(e);
	if (elem == null) return true;
	if (elem.nodeName == "#text")
	  elem = elem.parentNode;
	cls = elem.className;
	if (cls != "" && (cls.indexOf("_o") == cls.length - 2 || 
			  cls.indexOf("_o ") > -1)) {
		arr = cls.split(" ");
		str = "";
		for (var i=0; i<arr.length;i++) {
			cls = arr[i];
			if (cls.length < 2 || cls.indexOf("_o") != cls.length-2) 
				str=str+" "+cls;
		}
		elem.className = str.substring(1,str.length);
	}
	return true;
}
function confirmSub(elem, msg) {
	result = confirm(msg);
	nosub = result ? '' : elem.id;
	return result;
}

function _click(e) {
	e = getEvent(e);
	var elem = eventTarget(e);
	if (elem.nodeName == "#text")
	  elem = elem.parentNode;
	bid = elem.id;
	if (elem.value!="Close" && elem.value!="Help" && elem.value!="Export" && elem.value!="Print" && elem.value!="Download" && elem.value!="Statement Preferences" && elem.value!="eStatement List" && elem.value!="More" && elem.value!="GO" && elem.value!="SMS" && elem.value!="Other Services" && elem.value!="BOQ Contacts" && !(elem.classList.contains("enableMultiSubmit"))) {
	if (!submitted) { 
		if (bid != null && bid != nosub && bid.length>3 && bid.substring(0,3) == 'bu_') 	return subWith(bid, e);
		else if ((elem.type == "submit" || elem.type == "image" || (elem.tagName.toLowerCase() == "a" && !containsInPageAnchorRef(elem.href))) && result ) { submitted = true; }
	} else { 
		if (elem.type == "submit" || elem.type == "image" || (elem.tagName.toLowerCase() == "a" && !containsInPageAnchorRef(elem.href))) {
			alert('Your previous request is processing.  Please wait.'); return false; }}}
}

function containsInPageAnchorRef(str) {
	if (str == null || str.length == 0) {
		return false;
	} else if (str.indexOf("#") == 0) {
		return true;
	} else if (str.indexOf(window.location.href) >= 0) {
		return (str.indexOf("#") >= 0);
	} else {
		return false;
	}
}

function subW2(bid, e) {
	if (submitted) {
			alert('Your previous request is processing.  Please wait.');
			return false; }
	e = getEvent(e);
	if (e !=null) {
		var elem = eventTarget(e);
		if (elem.nodeName == "#text")
			elem = elem.parentNode;
			if (elem == null | (elem.value!="Close" && elem.value!="Help" && elem.value!="Export" && elem.value!="Print" && elem.value!="Download" && elem.value!="Statement Preferences" && elem.value!="eStatement List" && elem.value!="More" && elem.value!="GO" && elem.value!="SMS" && elem.value!="Other Services" && elem.value!="BOQ Contacts")) { submitted = true; }

		cancelEvent(e); // Stop bubbling
	}
	submitClicked = true;

	var frm = document.forms["SSF"];
	var hElem = frm.elements["ss_bid"];
	hElem.value = bid;

	frm.submit();
	return false;
}

function _keypress(event) {
	event = getEvent(event);
	var kCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
	if (kCode == 13 ) {
		var elem = event.srcElement ? event.srcElement : event.target;
		if (elem.type != 'textarea' ) { 
			if ((elem.tagName.toLowerCase() == "select" )||(elem.tagName.toLowerCase() == "input" && 
				(elem.type == "submit" || elem.type == "button" || elem.type == "image")) || elem.tagName.toLowerCase() == "a") {
				// ignore event if it due to an enter keypress on a submittable object
				return true;
			}
			return subWith("_enter", event); 
		}
	}
	if (kCode == 27 ) { return subWith("_esc", event);  }
	if(kCode == 116) {  f5Pressed= true; }
}


function _fieldkeypress(field, event) {
 if ((navigator.appName == 'Microsoft Internet Explorer')&&(parseFloat(navigator.appVersion)<8)) {
	event = getEvent(event);
	var kCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
	if ((kCode == 40 )||(kCode == 38)) {
		field.onchange();
		}
	return false;
	}
	else { return false;  }
}


function _exit() {
  var frm = document.forms["SSF"];
  win = open( frm.action + "&exit", "exit")
  win.close()
}

function checkWindowCloseEvent(btnValue) {
//[IBBOQ-3406]This is due to sending two requests instead of one when use presses f5.
if(f5Pressed == false){
	var  form , buttons;
	if (!submitClicked) {
		form = document.getElementById("SSF2Form");
		if (form != null)  {
		buttons = form.getElementsByTagName("input");
		for (i in buttons) { if (buttons[i].value==btnValue) {buttons[i].click()} }
	  	}
	}
}
}

function clearWindowCloseEvent() {
	submitClicked = true;
}

function cancelEvent(e)
{
  if(!e)
	e = window.event;
  if(e.stopPropagation)
	e.stopPropagation();
  if(e.preventDefault)
	e.preventDefault();
  e.cancelBubble = true;
  e.cancel = true;
  e.returnValue = false;
  return false;
}

function endsWith (str, phrase) {
	return (str.substring(str.length-phrase.length, str.length) == phrase);
}
function startsWith (str, phrase) {
	return (str.substring(0, phrase.length) == phrase);
}

document.onclick=_click;
document.onmouseover=_over;
document.onmouseout=_out;
//document.onkeypress = _keypress;
//document.onunload=_exit;

/* The following functions are (c) 2000 by John M Hanna and are
 * released under the terms of the Gnu Public License.
 * You must freely redistribute them with their source -- see the
 * GPL for details.
 *  -- Latest version found at http://sourceforge.net/projects/shop-js
 */
bs=28;
bx2=1<<bs; bm=bx2-1; bx=bx2>>1; bd=bs>>1; bdm=(1 << bd) -1
log2=Math.log(2)


function badd(a,b) {
 var al=a.length, bl=b.length
 if(al < bl) return badd(b,a)
 var c=0, r=[], n=0
 for(; n<bl; n++) {
  c+=a[n]+b[n]
  r[n]=c & bm
  c>>>=bs
 }
 for(; n<al; n++) {
  c+=a[n]
  r[n]=c & bm
  c>>>=bs
 }
 if(c) r[n]=c
 return r
}
function beq(a,b) {
 if(a.length != b.length) return 0
 for(var n=a.length-1; n>=0; n--)
  if(a[n] != b[n]) return 0
 return 1
}
function bsub(a,b) {
 var al=a.length, bl=b.length, c=0, r=[]
 if(bl > al) {return []}
 if(bl == al) {
  if(b[bl-1] > a[bl-1]) return []
  if(bl==1) return [a[0]-b[0]]
 }
 for(var n=0; n<bl; n++) {
  c+=a[n]-b[n]
  r[n]=c & bm
  c>>=bs
 }
 for(;n<al; n++) {
  c+=a[n]
  r[n]=c & bm
  c>>=bs
 }
 if(c) {return []}
 if(r[n-1]) return r
 while(n>1 && r[n-1]==0) n--;
 return r.slice(0,n)
}
function bmul(a,b) {
 b=b.concat([0])
 var al=a.length, bl=b.length, r=[], n,nn,aa, c, m
 var g,gg,h,hh, ghhb

 for(n=al+bl; n>=0; n--) r[n]=0
 for(n=0; n<al; n++) {
  if(aa=a[n]) {
   c=0
   hh=aa>>bd; h=aa & bdm
   m=n
   for(nn=0; nn<bl; nn++, m++) {
	g = b[nn]; gg=g>>bd; g=g & bdm
	//(gg*2^15 + g) * (hh*2^15 + h) = (gghh*2^30 + (ghh+hgg)*2^15 +hg)
	ghh = g * hh + h * gg
	ghhb= ghh >> bd; ghh &= bdm
	c += r[m] + h * g + (ghh << bd)
	r[m] = c & bm
	c = (c >> bs) + gg * hh + ghhb
   }
  }
 }
 n=r.length
 if(r[n-1]) return r
 while(n>1 && r[n-1]==0) n--;
 return r.slice(0,n)
}

function blshift(a,b) {
 var n, c=0, r=[]
 for(n=0; n<a.length; n++) {
  c|= (a[n]<<b) 
  r[n]= c & bm
  c>>=bs
 }
 if(c) r[n]=c
 return r
}
function brshift(a) {
 var c=0,n,cc, r=[]
 for(n=a.length-1; n>=0; n--) {
  cc=a[n]; c<<=bs
  r[n]=(cc | c)>>1
  c=cc & 1
 }
 while(r.length > 1 && r[r.length-1]==0) {
  r=r.slice(0,-1)
 }
 this.a=r; this.c=c
 return this
}
function zeros(n) {var r=[]; while(n-->0) r[n]=0; return r}
function toppart(x,start,len) { var n=0
 while(start >= 0 && len-->0) n=n*bx2+x[start--]
 return n
}
function bdiv(x,y) {
 var n=x.length-1, t=y.length-1, nmt=n-t
 if(n < t || n==t && (x[n]<y[n] || n>0 && x[n]==y[n] && x[n-1]<y[n-1])) {
  this.q=[0]; this.mod=x; return this
 }
 if(n==t && toppart(x,t,2)/toppart(y,t,2) <4) {
  var q=0, xx
  for(;;) {
   xx=bsub(x,y)
   if(xx.length==0) break
   x=xx; q++
  }
  this.q=[q]; this.mod=x; return this
 }
 var shift, shift2
 shift2=Math.floor(Math.log(y[t])/log2)+1
 shift=bs-shift2
 if(shift) {
  x=x.concat(); y=y.concat()
  for(i=t; i>0; i--) y[i]=((y[i]<<shift) & bm) | (y[i-1] >> shift2); y[0]=(y[0]<<shift) & bm
  if(x[n] & ((bm <<shift2) & bm)) { x[++n]=0; nmt++; }
  for(i=n; i>0; i--) x[i]=((x[i]<<shift) & bm) | (x[i-1] >> shift2); x[0]=(x[0]<<shift) & bm
 }
 var i, j, x2, y2,q=zeros(nmt+1)

 y2=zeros(nmt).concat(y)
 for(;;) {
  x2=bsub(x,y2)
  if(x2.length==0) break
  q[nmt]++
  x=x2
 }
 var yt=y[t], top=toppart(y,t,2)
 for(i=n; i>t; i--) {
  m=i-t-1
  if(i >= x.length)
   q[m]=1
  else if(x[i] == yt)
   q[m]=bm
  else
   q[m]=Math.floor(toppart(x,i,2)/yt)

  topx=toppart(x,i,3)
  while(q[m] * top > topx)
   q[m]--
  y2=y2.slice(1)
  x2=bsub(x,bmul([q[m]],y2))
  if(x2.length==0) {
   q[m]--
   x2=bsub(x,bmul([q[m]],y2))
  }
  x=x2
 }
 if(shift) {
  for(i=0; i<x.length-1; i++) x[i]=(x[i]>>shift) | ((x[i+1] << shift2) & bm); x[x.length-1]>>=shift
 }
 while(q.length > 1 && q[q.length-1]==0) q=q.slice(0,q.length-1)
 while(x.length > 1 && x[x.length-1]==0) x=x.slice(0,x.length-1)
 this.mod=x
 this.q=q
 return this
}

function bmod(p,m) {
 if(m.length==1) {
  if(p.length==1) return [p[0] % m[0]]
  if(m[0] < bdm) return [simplemod(p,m[0])]
 }

 var r=bdiv(p,m)
 return r.mod
}
function simplemod(i,m) {
 if(m>bdm)
  return mod(i,[m])
 var c=0, v
 for(var n=i.length-1; n>=0; n--) {
  v=i[n]
  c=((v >> bd) + (c<<bd)) % m
  c=((v & bdm) + (c<<bd)) % m
 }
 return c
}
function bmodexp(xx,y,m) {
 var r=[1], n, an,a, x=xx.concat()
 var mu=[]
 n=m.length*2;  mu[n--]=1;  for(; n>=0; n--) mu[n]=0; mu=bdiv(mu,m).q

 for(n=0; n<y.length; n++) {
  for(a=1, an=0; an<bs; an++, a<<=1) {
   if(y[n] & a) r=bmod2(bmul(r,x),m,mu)
   x=bmod2(bmul(x,x),m,mu)
  }
 }
 return r
}
function bmod2(x,m,mu) {
 var xl=x.length - (m.length << 1)
 if(xl > 0) {
  return bmod2(x.slice(0,xl).concat(bmod2(x.slice(xl),m,mu)),m,mu)
 }
 var ml1=m.length+1, ml2=m.length-1,rr
 var q3=bmul(x.slice(ml2),mu).slice(ml1)
 var r1=x.slice(0,ml1)
 var r2=bmul(q3,m).slice(0,ml1)
 var r=bsub(r1,r2)
 if(r.length==0) {
  r1[ml1]=1
  r=bsub(r1,r2)
 }
 for(var n=0;;n++) {
  rr=bsub(r,m)
  if(rr.length==0) break
  r=rr
  if(n>=3) return bmod2(r,m,mu)
 }
 return r
}

function sub2(a,b) {
 var r=bsub(a,b)
 if(r.length==0) {
  this.a=bsub(b,a)
  this.sign=1
 } else {
  this.a=r
  this.sign=0
 }
 return this
}
function signedsub(a,b) {
 if(a.sign) {
  if(b.sign) {
   return sub2(b,a)
  } else {
   this.a=badd(a,b)
   this.sign=1
  }
 } else {
  if(b.sign) {
   this.a=badd(a,b)
   this.sign=0
  } else {
   return sub2(a,b)
  }
 }
 return this
}

function modinverse(x,n) {
 var y=n.concat(), t, r, bq, a=[1], b=[0], ts
 a.sign=0; b.sign=0
 while( y.length > 1 || y[0]) {
  t=y.concat()
  r=bdiv(x,y)
  y=r.mod
  q=r.q
  x=t
  t=b.concat(); ts=b.sign
  bq=bmul(b,q)
  bq.sign=b.sign
  r=signedsub(a,bq)
  b=r.a; b.sign=r.sign
  a=t; a.sign=ts
 }
 if(beq(x,[1])==0) return [0]
 if(a.sign) {
  a=bsub(n,a)
 }
 return a
}

function crt_RSA(m, d, p, q) {
 var xp = bmodexp(bmod(m,p), bmod(d,bsub(p,[1])), p)
 var xq = bmodexp(bmod(m,q), bmod(d,bsub(q,[1])), q)
 var t=bsub(xq,xp);
 if(t.length==0) {
  t=bsub(xp,xq)
  t = bmod(bmul(t, modinverse(p, q)), q);
  t=bsub(q,t)
 } else {
  t = bmod(bmul(t, modinverse(p, q)), q);
 } 
 return badd(bmul(t,p), xp)
}

function t2b(s) {
 var bits=s.length*8, bn=1, r=[0], rn=0, sn=0, sb=1;
 var c=s.charCodeAt(0)
 for(var n=0; n<bits; n++) {
  if(bn > bm) {bn=1; r[++rn]=0; }
  if(c & sb)  r[rn]|=bn;
  bn<<=1
  if((sb<<=1) > 255) {sb=1; c=s.charCodeAt(++sn); }
 }
 return r;
}
function b2t(b) {
 var bits=b.length*bs, bn=1, bc=0, r=[0], rb=1, rn=0
 for(var n=0; n<bits; n++) {
  if(b[bc] & bn) r[rn]|=rb;
  if((rb<<=1) > 255) {rb=1; r[++rn]=0; }
  if((bn<<=1) > bm) {bn=1; bc++; }
 }
 while(r[rn]==0) {rn--;}
 var rr=''
 for(var n=0; n<=rn; n++) rr+=String.fromCharCode(r[n]);
 return rr;
}
b64s='abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_"'
function textToBase64(t) {
 var r=''; var m=0; var a=0; var tl=t.length-1; var c
 for(n=0; n<=tl; n++) {
  c=t.charCodeAt(n)
  r+=b64s.charAt((c << m | a) & 63)
  a = c >> (6-m)
  m+=2
  if(m==6 || n==tl) {
   r+=b64s.charAt(a)
   if((n%45)==44) {r+="\n"}
   m=0
   a=0
  }
 }
 return r
}
//function rsaEncode(key,mod,text) {
// return textToBase64(b2t(bmodexp(t2b(text+String.fromCharCode(0,2)),key,mod)))
//}
function encWid(key,mod,frm,spid,wn) {
 wid=frm.elements[wn]
 rndm=frm.elements[spid].value
 clr=rndm.substr(11,8) + wid.value
 if (wid.value.length>0) {
  frm.elements[wn+"_RSA"].value=textToBase64(b2t(bmodexp(t2b(clr+String.fromCharCode(0,2)),key,mod)))
  wid.value = ""
 }
}
// Hidden Layers
function showLayer(idName, displayType){
	if(!displayType)
		displayType = "block";
	element = document.getElementById(idName);
	element.style.display = displayType;
}

function hideLayer(idName){
	element = document.getElementById(idName);
	if(!element.onmouseover)
		element.style.display = "none";
}

// show Actions:
function showActions(idName){
	var browserName=navigator.appName; 

	// IE doesnt support table-row
	if (browserName=="Microsoft Internet Explorer"){  
 		showLayer(idName, "block")
	}
	else
		showLayer(idName, "table-row");

	// wait some seconds before hiding the actions
	setTimeout("hideLayer('" + idName + "')", 60000 );
}	

// hide Actions:
function hideActions(idName){
	hideLayer(idName);
}

//***********************************************************
//
// Search Filter for data table
//
//***********************************************************
//
// go through the data table and put all the data into an array (so we can search faster)
//
// !! The table has to have an id named "dataTable" (id="table") !!
var dataArray; // array filled with table contents
var tableId;
var rows = new Array(); // the table rows

function initTable(id){
	tableId = id;
	dataArray = new Array();
	var table = document.getElementById(tableId);
	rows = table.getElementsByTagName("tr");

	//all this i think is not being used.
	for( var i = 0; i < rows.length; i++){ 
		// add id to each row so that its easier to find them later on
		dataArray[i] = new Array();
		for( var j = 0; j < rows[i].cells.length; j++){
			dataArray[i][j] = rows[i].cells[j].innerHTML;
		}
	}

	//set the css class to mark rows that are checked as selected
	for( var i = 1; i < rows.length; i+=2){  // skip the action rows and skip the footer row. Note if footer will need to exclude this too.
		select_row( rows[i].cells[0].getElementsByTagName("input")[0], true);   //assume select row check box is in first cell
	}
}

function initBatchesTable(id,sourceColumn){
	tableId = id;
	dataArray = new Array();
	var table = document.getElementById(tableId);
	rows = table.getElementsByTagName("tr");

	for( var i = 0; i < rows.length; i++){ 
		// add id to each row so that its easier to find them later on
		dataArray[i] = new Array();
		for( var j = 0; j < rows[i].cells.length; j++){
			dataArray[i][j] = rows[i].cells[j].innerHTML;
		}
	}

	//set the css class to mark rows that are checked as selected
	for( var i = 1; i < rows.length; i+=2){  // skip the action rows and skip the footer row. Note if footer will need to exclude this too.
		select_row_batches( rows[i].cells[0].getElementsByTagName("input")[0], true,sourceColumn);   //assume select row check box is in first cell
	}
	//If Javascript is enabled display total and select counters.
	element = document.getElementById("total_selected");
	if(element){
		cssClass 
= element.className;

		element.className = cssClass.replace(/hidden/, "'");
	}
	element = document.getElementById("totalPymt");
	if(element){
		cssClass 
= element.className;
		element.className = cssClass.replace(/hidden/, "'");
	}
}

// 
// This functions fires when user hits "search". It tries to match the search string with values in the cells (of a specified column) and hides all the rows that do not match
//

var hiddenRows;

function filterTable(searchString){
	// clear search and start a new one:
	clearSearch();

	if(!searchString)
		searchString = document.getElementById("searchString").value; // the search string
	
	if(!searchString){
		clearSearch();
		return false;
	}

	regEx = new RegExp('.*' + searchString + '.*', 'i'); // make a regex outof it
	feedbackDiv = document.getElementById("feedback"); // feedback div - used to display "no results found" if that is the case

	//searchColumn = document.getElementById("search_column"); // which of the columns we are searching
	var column = 1; //searchColumn.value;

	// keep track of the rows we hide so that it"s easy to unhide them
	hide = new Array();

	// search the array holding the table data:
	count = 0;
	for( var i = 1; i < dataArray.length; i += 2){  // start with no 1 to skip headers - plus skip action rows - stop before footer

		// hide all the rows that dont match
		if(dataArray[i][column].match(regEx)){
			count++;
		}
		else // hide the ones that didnt match
			hide.push(i); //matches.push(dataArray[i]);
	}

	// display result table and
	// show feedback message if no records are found
	if(count > 0){
		// show the results table:
		hideRows(hide);
		// hide any feedback messages:
		feedbackDiv.innerHTML = "";
		feedbackDiv.style.display = "none";
	}
	else{
		// show feedback - nothing found.
		feedbackDiv.innerHTML = "<strong>No records found</strong>"
		feedbackDiv.style.display = "block";
	}
}

// show only selected rows
function showSelected(){
	clearSearch();
	hide = new Array();

	for( var i = 1; i < rows.length; i++){ 
		// add id to each row so that its easier to find them later on:
		if(rows[i].cells[0].firstChild.type == "checkbox" && !rows[i].cells[0].firstChild.checked)
			hide.push(i);
	}
	hideRows(hide);
}


// unhide all the hidden rows
function clearSearch(){
	for( var i = 1; i < rows.length; i += 2){
		 cssClass = rows[i].className;
		rows[i].className = cssClass.replace(/hidden/, "''");
	}
}

function hideRows(array){ // just hide rows
	for(i = 0; i < array.length; i++){
		rows[array[i]].className += " hidden";
	}	
}

// global count for how many has been selected
var select_count = 0;

//handle selected/unselected row for the batches payees. Includes display of total and nb of selected payees
function select_row_batches(element, onLoad,sourcecolumn){

	//ignore blank rows, undefined element
	if(element){
		select_row(element, onLoad);
		// show number of selected payees
		var initial_count = parseInt(document.getElementById("previous_selected").innerHTML);
		var count = select_count + initial_count;
		document.getElementById("total_selected").innerHTML = count + " selected";

		calculateTotal("payees_table", "totalLabel", sourcecolumn);
		activate_or_deactive_row(element, sourcecolumn);
	}
}

//activate/deactive elements in a selected/unselected row
function select_row(element, onLoad) {

		//ignore blank rows, undefined element
	if(element){
		tr = element.parentNode.parentNode;

		if(element.checked){
			tr.className = "flrck";
			if(!onLoad)
				select_count++;
		} else{
		  tbLength = element.parentNode.parentNode.parentNode.getElementsByTagName("tr").length;
	 		tr = element.parentNode.parentNode;
   		i=1;
   		ind=0;
  	 	while (tr != element.parentNode.parentNode.parentNode.getElementsByTagName("tr")[ind] && ind<=tbLength )
   		{
			if(  element.parentNode.parentNode.parentNode.getElementsByTagName("tr")[ind].className != "action_row") //action row must not take in count for add or even css for rows
	  	{i++};
	  		ind++;
  	 }
		   if ( i%2 == 0)
		   {
				tr.className = "flr";
			}
			else{		
					tr.className = "flro";
			}			
			if (ind ==0)
			{	
			    tr.className = tr.className + " first";
			}
			else
			{
				if (ind==tbLength-1 )
				{
			      tr.className = tr.className + " last";
				} else(ind==tbLength-2 )
				{
				  element.parentNode.parentNode.parentNode.getElementsByTagName("tr")[ind+1].className != "action_row"
			      tr.className = tr.className + " last";
				}
			}	
		if(!onLoad)
			select_count--;
		}
	}
};

//activate/deactive elements in a selected/unselected row. Designed for batches screen
function activate_or_deactive_row(element,sourceColumn){
	tr = element.parentNode.parentNode;
	
	column = 2;  //this is the reference column in the batch view
	column2 = sourceColumn-1; //this is either the regular pay column (when displayed) or the this pay column in the batch view
	column3 = sourceColumn; // this is the this pay column in the batch view
		
	// get the input fields in the columns
	var inputField1 = tr.cells[column].getElementsByTagName("input")[0];
	var inputField2 = tr.cells[column2].getElementsByTagName("input")[0];
	var inputField3 = tr.cells[column3].getElementsByTagName("input")[0];
	
	if(element.checked){
		// enable the input fields
		inputField1.disabled = false;
		if (inputField2 !== undefined) {
			inputField2.disabled = false;
		};
		if (inputField3 !== undefined) {
			inputField3.disabled = false;
		};
		inputField1.className ="";
		if (inputField3 !== undefined) {
			inputField3.className ="";
		};
		if (inputField2 !== undefined) {
			if(inputField2.type=="button") {
				inputField2.className ="flblinkbtn_";
			} else {
				inputField2.className ="";
			};
		};
	}
	else{
		// disable the input fields
		inputField1.disabled = true;
		if (inputField2 !== undefined) {
			inputField2.disabled = true;
		};
		if (inputField3 !== undefined) {
			inputField3.disabled = true;
		};
		inputField1.className ="inputDisabled";
		if (inputField2 !== undefined) {
			inputField2.className ="inputDisabled";
		};
		if (inputField3 !== undefined) {
			inputField3.className ="inputDisabled";
		};
	}
};

// add up and display total. Designed for batches screen
function calculateTotal(tableId, targetId, column){
	if(!document.getElementById)
		return false; // old browsers not supporting this function, exit		

	table = document.getElementById(tableId);
	rows = table.getElementsByTagName("tr"); // retrieve all the input fields
	target = document.getElementById(targetId);

	if(!column)
		column = 1;

	//record the existing total ie. of previously selected payees not displayed
	var previous_total = parseFloat(document.getElementById("previous_total").innerHTML);
	total = previous_total;
	for(row = 1; row < rows.length - 1; row += 2){ // skip the action rows and skip the footer row
		var cells = rows[row].getElementsByTagName("td");
		var checkInput = cells[0].getElementsByTagName("input")[0]; //assume check box is in the first column
		if(checkInput){
			if(checkInput.checked){ //skip rows not selected by the check box.
				inputs = cells[column].getElementsByTagName("input");
				if(inputs[0].value) {
					if(inputs[0].value.charAt(0) == "$") {
						inputVal = inputs[0].value.substring(1).replace(/,/g,"");
					} else{
						inputVal = inputs[0].value.replace(/,/g,"");
					}
					if(inputVal == parseFloat(inputVal))
						total += parseFloat(inputVal);
				}
			}
		}
	}	
	target.innerHTML = "$" + total.toFixed(2);
};

// Figure out what browser we are running
var BrowserDetect = {
	init: function () {
		this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
		this.version = this.searchVersion(navigator.userAgent)
			|| this.searchVersion(navigator.appVersion)
			|| "an unknown version";
		this.OS = this.searchString(this.dataOS) || "an unknown OS";
	},

	searchString: function (data) {
		for (var i=0;i<data.length;i++)	{
			var dataString = data[i].string;
			var dataProp = data[i].prop;
			this.versionSearchString = data[i].versionSearch || data[i].identity;
			if (dataString) {
				if (dataString.indexOf(data[i].subString) != -1)
					return data[i].identity;
			}
			else if (dataProp)
				return data[i].identity;
		}
	},

	searchVersion: function (dataString) {
		var index = dataString.indexOf(this.versionSearchString);
		if (index == -1) return;
		return parseFloat(dataString.substring(index+this.versionSearchString.length+1));
	},

	dataBrowser: [
		{ 	string: navigator.userAgent,
			subString: "OmniWeb",
			versionSearch: "OmniWeb/",
			identity: "OmniWeb"
		},
		{
			string: navigator.vendor,
			subString: "Apple",
			identity: "Safari"
		},
		{
			prop: window.opera,
			identity: "Opera"
		},
		{
			string: navigator.vendor,
			subString: "iCab",
			identity: "iCab"
		},
		{
			string: navigator.vendor,
			subString: "KDE",
			identity: "Konqueror"
		},
		{
			string: navigator.userAgent,
			subString: "Firefox",
			identity: "Firefox"
		},
		{
			string: navigator.vendor,
			subString: "Camino",
			identity: "Camino"
		},
		{		// for newer Netscapes (6+)
			string: navigator.userAgent,
			subString: "Netscape",
			identity: "Netscape"
		},
		{
			string: navigator.userAgent,
			subString: "MSIE",
			identity: "Explorer",
			versionSearch: "MSIE"
		},
		{
			string: navigator.userAgent,
			subString: "Gecko",
			identity: "Mozilla",
			versionSearch: "rv"
		},
		{ 		// for older Netscapes (4-)
			string: navigator.userAgent,
			subString: "Mozilla",
			identity: "Netscape",
			versionSearch: "Mozilla"
		}
	],
	dataOS : [
		{
			string: navigator.platform,
			subString: "Win",
			identity: "Windows"
		},
		{
			string: navigator.platform,
			subString: "Mac",
			identity: "Mac"
		},
		{
			string: navigator.platform,
			subString: "Linux",
			identity: "Linux"
		}
	]
};
BrowserDetect.init();

//handle enable/disable of option fields of ExapandableBox for the DUA - users tab
function showOptionsFor(element) {

	//This is done so that still works if Java script is disabled
	if(element){
		var elementId = element.name;
		var inputId = elementId.replace(/radio/, "options_container");
		var inputElement = document.getElementById(inputId);
		if(inputElement) {
			var cssClass = inputElement.className;
			//remove the hidden class and then add it to prevent it being added multiple times.
			var cssNewClass = cssClass.replace(/inactive/, "");

			if(element.value == 'None'){
				inputElement.className = cssNewClass + " inactive";
			} else {
				inputElement.className = cssNewClass;
			}
		}
	}
};

//handle selected/unselected limit input field on the DUA - users tab
function showLimitFor(element) {

	//This is done so that still works if Java script is disabled
	if(element){
		var elementId = element.name;
		var inputId = elementId.replace(/radio/, "input");
		var inputElement = document.getElementById(inputId);
		if(inputElement) {
			var cssClass = inputElement.className;
			//remove the hidden class and then add if required it to prevent it being added multiple times.
			var cssNewClass = cssClass.replace(/hidden/, "");

			if(element.value == 'Execute'){
				inputElement.className = cssNewClass;
			} else {
				inputElement.className = cssNewClass + " hidden";
			}
		}
	}
};

//handle selected/unselected limit input field on the DUA - users tab
function showFieldForCheckBox(element) {

	//This is done so that still works if Java script is disabled
	if(element){
		var elementId = element.name;
		var inputId = elementId.replace(/chk/, "input");
		var inputElement = document.getElementById(inputId);
		if(inputElement) {
			var cssClass = inputElement.className;
			//remove the hidden class and then add if required it to prevent it being added multiple times.
			var cssNewClass = cssClass.replace(/hidden/, "");

			if(element.checked){
				inputElement.className = cssNewClass;
			} else {
				inputElement.className = cssNewClass + " hidden";
			}
		}
	}
};

//handle selected/unselected limit input field on the DUA - users tab
function initDua(){

	var elems = document.getElementsByTagName("input");
	for( var i = 0; i < elems.length; i++) {
		var inputName = elems[i].name;
		var strStart = inputName.substring(0, 4);
		//dont hide those that should be displayed
		if (strStart == "txt_") {
			//retrieve the related radio button
			var radioName = inputName.replace(/txt/, "radio");
			var radioElems = document.getElementsByName(radioName);
			if (radioElems) {
				//ensure that the input box is only hidden if the radio button does is not selected for the show options

				var shouldShow = false;
				for( var e = 0; e < radioElems.length; e++) {
					if (radioElems[e].value == 'Execute') {
						if (radioElems[e].checked == true) {
							shouldShow = true;
						}
					}
				}
				if (shouldShow == false) {
					var divName = inputName.replace(/txt/, "input");
					var divElem = document.getElementById(divName);
					divElem.className = divElem.className + " hidden";
				}
			}
		}
	}
};
function toggleTable(element, tbl_id){
// {tbl_id} is a string that is the unique id for table to be toggled
// In order for toggling to work, the table must have exptbl_hdr_{tbl_id} and exptbl_{tbl_id} elements.
// {element} is the id of the calling element (i.e. the link element)

var hdrId = "exptbl_hdr_" + tbl_id;
var bodyId = "exptbl_" + tbl_id;

if (element.innerHTML=="[Show]")
{document.getElementById(hdrId).className="exptbl_expanded";
element.innerHTML="[Hide]";
showLayer(bodyId);
element.blur();}
else 
{document.getElementById(hdrId).className="exptbl_unexpanded";
element.innerHTML="[Show]";
hideLayer(bodyId);
element.blur();}
};
function initPendingAuth(){

// By default, all (JavaScript-only) links on the page have no text (invisible)
// This function parses the document tree and locates any such JavaScript links, and
// adds text so the links become functional on JS capable user agents.

var elems = document.getElementsByTagName("a");
	for( var i = 0; i < elems.length; i++) {
		var inputName = elems[i].id;
		var strStart = inputName.substring(0, 12);
		//Add the maximise button and change the default table state
		if (strStart == "exptbl_link_") {
			var strEnd = inputName.substring(12);
			elems[i].innerHTML="[Hide]";
			toggleTable(elems[i],strEnd);
		}
	}
};

//method used to reset an expandable list box when the cancel button has been clicked
function hideAndResetLayer(idName) {

	document.getElementById("edit_tab_" + idName).className="edit";
	hideLayer("expand_" + idName);
	showLayer(idName); 

	var elems = document.getElementsByTagName("input");
	
	for(i=0; i<elems.length; i++)
	{
		var inputName = elems[i].name;
		try {
			if (startsWith(inputName, "nohide_txt_" + idName)) {
				elems[i].value = elems[i].defaultValue;	
			}
			if (startsWith(inputName, "txt_" + idName)) {
				elems[i].value = elems[i].defaultValue;	
			}
			if (startsWith(inputName, "radio_" + idName)) {
				var alreadyChecked = elems[i].checked;
				elems[i].checked = elems[i].defaultChecked;
				if (elems[i].checked) {
					if (!alreadyChecked) {
						elems[i].click();
					}
				}
			}
			if (startsWith(inputName, "chk_" + idName) || startsWith(inputName, "cBox_" + idName)) {
				if (elems[i].defaultChecked) {
					if (!elems[i].checked) {
						elems[i].click();
					}
				} else {
					if (elems[i].checked) {
						elems[i].click();
					}
				}				
			}
		} catch (e) {
			//do nothing?
		}
	}	
	
};


//method used to reset an expandable list box when the cancel button has been clicked
function hideAndResetLayer(idName) {

	document.getElementById("edit_tab_" + idName).className="edit";
	hideLayer("expand_" + idName);
	showLayer(idName); 

	var elems = document.getElementsByTagName("input");
	
	for(i=0; i<elems.length; i++)
	{
		var inputName = elems[i].name;
		try {
			if (startsWith(inputName, "nohide_txt_" + idName)) {
				elems[i].value = elems[i].defaultValue;	
			}
			if (startsWith(inputName, "txt_" + idName)) {
				elems[i].value = elems[i].defaultValue;	
			}
			if (startsWith(inputName, "radio_" + idName)) {
				var alreadyChecked = elems[i].checked;
				elems[i].checked = elems[i].defaultChecked;
				if (elems[i].checked) {
					if (!alreadyChecked) {
						elems[i].click();
					}
				}
			}
			if (startsWith(inputName, "chk_" + idName) || startsWith(inputName, "cBox_" + idName)) {
				if (elems[i].defaultChecked) {
					if (!elems[i].checked) {
						elems[i].click();
					}
				} else {
					if (elems[i].checked) {
						elems[i].click();
					}
				}				
			}
		} catch (e) {
			//do nothing?
		}
	}	
	
};


//will validate all the currency values. should be used for the dua template gather page, and dua edit access details page
function checkAllCurrencyValues (currencySymbol) {
	var elems = document.getElementsByTagName("input");
	var foundError = false;
	var elemError;
	for(var i=0; i<elems.length && !foundError; i++) {
		if (!elems[i].disabled && elems[i].className != "hidden" && elems[i].type == "text") {
			var inputName = elems[i].name;
			if (startsWith(inputName, "nohide_txt_") || startsWith(inputName, "txt_")) {
				var elemValue = validateRestrictedString(elems[i].value);
				var currencyCheck = validateCurrency(elemValue, currencySymbol);
				if (currencyCheck.length > 1) {
					elemError = elems[i];
					elems[i].value = elemValue;
					foundError = true;
				}
			}
		}
	}
	
	if (foundError) {
		var errDiv = document.getElementById("errMsgDisplay");
		if (errDiv == null) {
			errContainerDiv = document.getElementById("errMsgTop")
			errDiv = document.createElement("div");
			errDiv.setAttribute("class","errmsg");
			errDiv.setAttribute("id","errMsgDisplay");
			errContainerDiv.appendChild(errDiv);
		};
		var label = getLabelForId(elemError.id);
		errDiv.innerHTML = label.innerHTML + currencyCheck;
		alert (label.innerHTML + currencyCheck);
		elemError.focus();
		return false;
	} else {
		return true;
	}
	
}

function getLabelForId(id) {
 var label, labels = document.getElementsByTagName("label");
 for (var i = 0; (label = labels[i]); i++) {
   if (label.htmlFor == id) {
	 return label;
   }
 }
 return false;
} 

//if currencyValue is in a valid currency format, then an empty string is returned.
//if currencyValue is not in a valid currency format, then an error string to be displayed to the user is returned
function validateCurrency(currencyValue, currencySymbol) {
	var trimmed = trimBlanks(currencyValue);
	if (trimmed.length == 0) {
		return "";
	}
	if (startsWith(trimmed, currencySymbol)) {
		trimmed = trimmed.substring(currencySymbol.length, trimmed.length);
	}
	//should use regular expression for this
	for (var i=0; i<trimmed.length;i++) {
		var currentChar = trimmed.charAt(i);
		if (!isDigit(currentChar) && (currentChar != ".") && (currentChar != ",")) {
			return currencyValue + " contains invalid currency characters";
		}
	}
	var periodIndex = trimmed.indexOf(".")
	if (!checkCommaPlacement(trimmed, periodIndex)) {
		return currencyValue + " should contain a comma before each group of 3 digits";
	}
	if ((periodIndex > -1) && ((periodIndex != trimmed.length - 3) || (trimmed.lastIndexOf(".") != periodIndex))) {
		return currencyValue + " should contain a decimal point followed by 2 digits";
	}
	return "";
	
};

//will trim any white spaces from the begining and end of a string
function trimBlanks (str) {
	return str.replace(/^\s*/, "").replace(/\s*$/, "");
}

function isDigit(num) {
	var string="1234567890";
	if (string.indexOf(num)!=-1){return true;}
	return false;
	}

//This function sets the value of regular pay to this pay of a particular payee
function set_this_pay(regularPayColumn, thisPayName){
	 var thisPayColumns = document.getElementsByName(thisPayName);
	 thisPayColumns[0].value = regularPayColumn.value;
	calculateTotal("payees_payers_table", "totalLabel", 4);
}
	
function checkCommaPlacement(str, periodIndex) {
	var dollars;
	if (periodIndex == -1) {
		dollars = str
	} else {
		dollars = str.substr(0, periodIndex);
	}
	var segments = dollars.split(",");
	if (segments.length == 1) {
		return true;
	}
	segments.shift();
	for (var i=0; i<segments.length;i++) {
		if (segments[i].length != 3) {
			return false;
		}
	}
	return true;
}

//this will validate the string restrictions on entry fields
function validateRestrictedString(str) {
	var isRestrictInput = true;
	if (isRestrictInput == false) { 
		return str;
	}
	return filterRestrictedChars(str);
}

// this function removes restricted inputs (angle brackets and characters with equivalent unicode value below 32)
function filterRestrictedChars(str) {
	str = str.replace(/[<>]/g," ");
	for (var i=0; i < str.length; i++) {
		if (str.charCodeAt(i) < 32) {
			str = replaceCharAt(str,i," ");
		}
	}
	return str;
}

// this function replaces a character with a new character (newChar) at a specified index (i) of a given string (str)
function replaceCharAt(str, index, newChar) { 
	return str.substr(0,index) + newChar + str.substr(index+1);
};

//The succeeding two functions show/hide and enable/disable a given field
//Value of the given field may also be provided
//This has been added in conjuction with [IBGEN-943/BL-636] issue on email confirmation option
function activate_or_deactivate_field_via_element(elementId, targetId, targetValue, shouldEnable){
	var elem = document.getElementById(elementId);
	 var target = document.getElementById(targetId);

	//ignore blank rows, undefined element
	if(elem.checked){
		target.className="";
		target.value=targetValue;
		if (shouldEnable) {
			target.disabled=false;
		} else {
			target.disabled=true;
		};
	} else{
		target.className="hidden";
	};
};

function activate_or_deactivate_field(element, targetId, targetValue, shouldEnable){
	activate_or_deactivate_field_via_element(element.id, targetId, targetValue, shouldEnable);
}
